# IA Explicables

> Comment interpréter et expliquer les résultats d'une intelligence artificielle ?

## References

- [Interpretable Machine Learning](https://christophm.github.io/interpretable-ml-book/)
- [Weapons of Math Destruction](https://en.wikipedia.org/wiki/Weapons_of_Math_Destruction)
- [MAIF Shapash](https://maif.github.io/shapash/)
